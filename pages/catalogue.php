<?php include '../blocks/header.php'; 
require_once DIR_PATH . 'classes/DB.php';
require_once DIR_PATH . 'classes/models/Conference.php';
require_once DIR_PATH . 'classes/models/Techno.php';
require_once DIR_PATH . 'classes/models/Theme.php';
require_once DIR_PATH . 'classes/models/Job.php';
require_once DIR_PATH . 'classes/models/Entreprise.php';

if ( ! is_logged_in() ) {
	header( 'location: ' . DIR_URL );
}

?>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
	<div class="container">
		<h1>Liste des conférences</h1>
		<p>Sélectionnez les conférences que vous souhaitez proposer au vote des internautes.</p>
	</div>
</div>

<?php
$conferences = \Conference::get_conferences();
?>

<div class="container">
    <div class="row" id="conferences-list">
        <?php foreach( $conferences as $conference ) : ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail" data-confid="<?php echo $conference->IDCONFERENCE ?>">
                    <img src="<?php echo DIR_URL ?>img/tech<?php echo rand( 1, 8 ); ?>.jpeg" alt="...">
                    <div class="caption">
                        <h3><?php echo $conference->TITRE; ?></h3>
                        
                        <p><?php echo $conference->CONTENU ?></p>
                        
	                    <?php if ( (int) $conference->SELECTION === 0 ) : ?>
                            <p>Selection : <span class="selection">Non</span></p>
                        <?php else : ?>
                            <p>Selection : <span class="selection">Oui</span></p>
                        <?php endif; ?>
                        
                        <button id="singleconf-btn" data-confid="<?php echo $conference->IDCONFERENCE ?>" class="singleconf-btn btn btn-primary">Voir</button>
	
	                    <?php if ( (int) $conference->SELECTION === 0 ) : ?>
                            <button data-confid="<?php echo $conference->IDCONFERENCE ?>" class="selection-btn btn btn-default">Autoriser le vote</button>
	                    <?php else: ?>
                            <button data-confid="<?php echo $conference->IDCONFERENCE ?>" class="deselection-btn btn btn-default">Ne pas autoriser le vote</button>
	                    <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php include '../blocks/footer.php'; ?>
