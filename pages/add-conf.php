<?php include '../blocks/header.php'; 
require_once '../classes/DB.php';
require_once '../classes/models/Conference.php';
require_once '../classes/models/Techno.php';
require_once '../classes/models/Theme.php';
require_once '../classes/models/Job.php';
require_once '../classes/models/Entreprise.php';

if ( isset( $_POST['confTitle'] ) ) {
    Conference::add_conference();
}

?>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
	<div class="container">
		<h1>Proposer une conférence</h1>
		<p>Vous pouvez proposer une conférence très facilement en remplissant le formulaire suivant</p>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-xs-12" id="add-conf">
            <form method="post" action="">
                
                <div class="form-group">
                    <label for="confTitle">Titre</label>
                    <input type="text" class="form-control" name="confTitle" id="confTitle" placeholder="Améliorer les performances d'une application C#">
                </div>
                
                <div class="form-group">
                    <label for="confDesc">Description</label>
                    <textarea name="confDesc" id="confDesc" class="form-control" rows="10"></textarea>
                </div>
                
                <div class="form-group">
                    <label for="confLevel">Niveau</label>
                    <select id="confLevel" class="form-control" name="confLevel">
                        <option value="100">Facile</option>
                        <option value="200">Moyen</option>
                        <option value="300">Difficile</option>
                    </select>
                </div>
            
                <?php $technologies = Techno::get_technos(); 
                if ( ! empty( $technologies ) ) : ?>
                    <div class="form-group">
                        <label for="confTools">Outils</label>
                        <select id="confTools" class="form-control" name="confTools">
                            <?php foreach ( $technologies as $techno ) : ?>
                                <option value="<?php echo $techno->IDTECHNO ?>"><?php echo $techno->NOMTECHNO ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>

                <?php $themes = Theme::get_themes(); 
                if ( ! empty( $themes ) ) : ?>
                    <div class="form-group">
                        <label for="confTheme">Thème</label>
                        <select id="confTheme" class="form-control" name="confTheme">
                            <?php foreach ( $themes as $theme ) : ?>
                                <option value="<?php echo $theme->IDTHEME ?>"><?php echo $theme->LIBELLETHEME; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>

                <?php $jobs = Job::get_jobs(); 
                if ( ! empty( $jobs ) ) : ?>
                    <div class="form-group">
                        <label for="confJob">Métier</label>
                        <select id="confJob" class="form-control" name="confJob">
                            <?php foreach ( $jobs as $job ) : ?>
                                <option value="<?php echo $job->IDMETIER ?>"><?php echo $job->LIBELLEMETIER; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label for="confName">Nom</label>
                    <input type="text" class="form-control" name="confName" id="confName" placeholder="Dupont">
                </div>
                
                <div class="form-group">
                    <label for="confFirstname">Prénom</label>
                    <input type="text" class="form-control" name="confFirstname" id="confFirstname" placeholder="Maxime">
                </div>
                
                <div class="form-group">
                    <label for="confTel">Téléphone</label>
                    <input type="text" class="form-control" name="confTel" id="confTel" placeholder="0624320432">
                </div>
                
                <div class="form-group">
                    <label for="confMail">Mail</label>
                    <input type="email" class="form-control" name="confMail" id="confMail" placeholder="xxx@hotmail.com">
                </div>
	
	            <?php $firms = Entreprise::get_entreprises();
	            if ( ! empty( $firms ) ) : ?>
                    <div class="form-group">
                        <label for="confFirm">Entreprise</label>
                        <select id="confFirm" class="form-control" name="confFirm">
				            <?php foreach ( $firms as $firm ) : ?>
                                <option value="<?php echo $firm->IDENTREPRISE ?>"><?php echo $firm->NOMENTREPRISE; ?></option>
				            <?php endforeach; ?>
                        </select>
                    </div>
	            <?php endif; ?>

                <button type="submit" class="btn btn-primary">Envoyer</button>
            </form>
        </div>
	</div>
</div>

<?php include '../blocks/footer.php'; ?>
