<?php
require_once 'C:\xampp\htdocs\ingetis_techdays\classes\DB.php';
require_once 'C:\xampp\htdocs\ingetis_techdays\classes\models\Conference.php';
require_once 'C:\xampp\htdocs\ingetis_techdays\classes\models\Niveau.php';
require_once 'C:\xampp\htdocs\ingetis_techdays\classes\models\Theme.php';
require_once 'C:\xampp\htdocs\ingetis_techdays\classes\models\Techno.php';
require_once 'C:\xampp\htdocs\ingetis_techdays\classes\models\Conferencier.php';

if ( isset( $_REQUEST['conf'] ) ) {
	$conference = \Conference::get_conference( $_REQUEST['conf'] );
	$conference = reset( $conference );
	ob_start();
	
	?>
	
	<div class="col-sm-12">
		<a class="btn btn-primary" href="http://127.0.0.1/ingetis_techdays/pages/catalogue.php">Précédent</a>
	</div>
	
	<div class="col-sm-6">
		<div class="thumbnail">
			
			<div class="caption">
				<h2><?php echo $conference->TITRE; ?></h2>
				
				<p><?php echo $conference->CONTENU ?></p>
				
				<?php if ( (int) $conference->SELECTION === 0 ) : ?>
					<p>Selection : <span class="selection">Non</span></p>
				<?php else : ?>
					<p>Selection : <span class="selection">Oui</span></p>
				<?php endif; ?>
				
				<p>Niveau : <?php echo \Niveau::get_level( $conference->IDNIVEAU ); ?></p>
				
				<p>Thème : <?php echo \Theme::get_theme( $conference->IDTHEME ); ?></p>
				
				<p>Parcours : <?php echo \Theme::get_parcours_by_theme( $conference->IDTHEME ); ?></p>
				
				<p>Technologie : <?php echo \Techno::get_techno_by_conf( $conference->IDCONFERENCE ); ?></p>
				
				<?php if ( ! is_null( $conference->NBVOTES ) ) : ?>
					<p>Nombre de votes : <?php echo $conference->NBVOTES; ?></p>
				<?php endif; ?>
				
				<?php if ( (int) $conference->SELECTION === 0 ) : ?>
					<button id="selection-btn" data-confid="<?php echo $conference->IDCONFERENCE ?>" class="selection-btn btn btn-default">Autoriser le vote</button>
				<?php else: ?>
					<button id="deselection-btn" data-confid="<?php echo $conference->IDCONFERENCE ?>" class="deselection-btn btn btn-default">Ne pas autoriser le vote</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<div class="col-sm-6">
		<div class="thumbnail">
			
			<div class="caption">
				
				<?php $conferencier = \Conferencier::get_conferencier( $_REQUEST['conf'] ); ?>
				
				<h2>Conférencier</h2>
				
				<p><?php echo $conferencier->NOM . ' ' . $conferencier->PRENOM; ?></p>
				
				<p>Mail : <?php echo $conferencier->MAIL ?></p>
				
				<p>Tel : <?php echo $conferencier->TELEPHONE ?></p>
				
				<p>Entreprise : <?php echo \Conferencier::get_entreprise( $conferencier->IDENTREPRISE ); ?></p>
				
			</div>
		</div>
	</div>
	
	<?php echo ob_get_clean();
}