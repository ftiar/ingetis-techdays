<?php

function login( $login = '', $pwd = '' ) {
	require_once DIR_PATH . 'classes\DB.php';
	
	$db     = new \DB( 'ingetis_techdays' );
	$req    = $db->get_PDO()->prepare( 'SELECT * from parametre WHERE login = :login AND mdp = :pwd' );
	$result = $req->execute( array(
		'login' => $login,
		'pwd'   => $pwd,
	) );
	
	$data = $req->fetchAll( \PDO::FETCH_OBJ );
	
	if ( is_array( $data ) && ! empty( $data ) ) {
		// on la démarre :)
		session_start();
		// on enregistre les paramètres de notre visiteur comme variables de session ($login et $pwd) (notez bien que l'on utilise pas le $ pour enregistrer ces variables)
		$_SESSION['login'] = $login;
		$_SESSION['pwd']   = $pwd;
		
		// on redirige notre visiteur vers une page de notre section membre
		header( 'location: ' . DIR_URL );
	}
}

function logout() {
	// On détruit les variables de notre session
	session_unset ();
	
	// On détruit notre session
	session_destroy ();
}

function is_logged_in() {
	if ( isset( $_SESSION['login'] ) ) {
		return true;
	}
	
	return false;
}