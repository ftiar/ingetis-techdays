
$( ".selection-btn" ).click(function() {

    var confid = $(this).attr("data-confid");
    var selectionBtn = $(this);

    $.ajax({
        url: '../inc/ajax-selection.php', // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: 'conf=' + confid,
        success: function( result, statut ) {
            console.log( 'AJAX OK' );
            $( 'span.selection' ).text( 'Oui' );
            $( "div.thumbnail[data-confid='" + confid + "']" ).append(
                $( '<div class="alert alert-success alert-dismissible" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Fermer"><span aria-hidden="true">&times;</span></button>Conférence sélectionnée, elle sera soumis aux votes des internautes !</div>' )
            );
            selectionBtn.parent().append( $( '<button data-confid="' + confid + '" class="deselection-btn btn btn-default">Ne pas autoriser le vote</button>' ) );
            selectionBtn.remove();
        }
    });
});

$( ".deselection-btn" ).click(function() {

    var confid = $(this).attr("data-confid");
    var deselectionBtn = $(this);

    $.ajax({
        url: '../inc/ajax-selection.php', // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: 'conf=' + confid,
        success: function( result, statut ) {
            console.log( 'AJAX OK' );
            $( 'span.selection' ).text( 'Non' );
            $( "div.thumbnail[data-confid='" + confid + "']" ).append(
                $( '<div class="alert alert-success alert-dismissible" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Conférence non sélectionnée, elle ne sera pas soumis aux votes des internautes !</div>' )
            );

            deselectionBtn.parent().append( $( '<button data-confid="' + confid + '" class="selection-btn btn btn-default">Autoriser le vote</button>' ) );
            deselectionBtn.remove();
        }
    });
});

$( ".singleconf-btn" ).click(function() {

    var confid = $(this).attr("data-confid");
    var singleconfBtn = $(this);

    $.ajax({
        url: '../inc/ajax-single-conf.php', // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: 'conf=' + confid,
        success: function( result, statut ) {
            console.log( 'AJAX SINGLE CONF OK' );
            $( "#conferences-list" ).html( result )
        }
    });
});
