<?php

class Conferencier {

	function __construct() {
	}

	public static function get_conferenciers() {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( 'SELECT * from conferencier' );
	}
	
	public static function get_conferencier( $confid ) {
		$db = new \DB( 'ingetis_techdays' );
		$result = $db->query( sprintf( 'SELECT IDCONFERENCIER from jouer WHERE IDCONFERENCE = %d', $confid ) );
		$conferencier_id = reset( $result )->IDCONFERENCIER;
		$result = $db->query( sprintf( 'SELECT * from conferencier WHERE IDCONFERENCIER = %d', $conferencier_id ) );
		return reset( $result );
	}
	
	public static function get_entreprise( $entreprise_id ) {
		$db = new \DB( 'ingetis_techdays' );
		$result = $db->query( sprintf( 'SELECT NOMENTREPRISE from entreprise WHERE IDENTREPRISE = %d', $entreprise_id ) );
		return reset( $result )->NOMENTREPRISE;
	}
	
}