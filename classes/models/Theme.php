<?php

require_once 'C:\xampp\htdocs\ingetis_techdays\classes\models\Parcours.php';

class Theme {

	function __construct() {
	}

	public static function get_themes() {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( 'SELECT * from theme' );
	}
	
	public static function get_theme( $theme_id = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( sprintf( 'SELECT LIBELLETHEME from theme WHERE IDTHEME = %d', $theme_id ) );
		return reset( $result )->LIBELLETHEME;
	}
	
	public static function get_parcours_by_theme( $theme_id = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( sprintf( 'SELECT IDPARCOURS from theme WHERE IDTHEME = %d', $theme_id ) );
		$parcours_id = reset( $result )->IDPARCOURS;
		return \Parcours::get_parcours( $parcours_id );
	}

}