<?php

class Niveau {

	function __construct() {
	}

	public static function get_levels() {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( 'SELECT * from niveau' );
	}
	
	public static function get_level( $level_id = 100 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( sprintf( 'SELECT NIVEAU from niveau WHERE IDNIVEAU = %d', $level_id ) );
		return reset( $result )->NIVEAU;
	}

}
