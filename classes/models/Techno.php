<?php

class Techno {

	function __construct() {
	}

	public static function get_technos() {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( 'SELECT * from technologie' );
	}
	
	public static function get_techno( $techno_id = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( sprintf( 'SELECT NOMTECHNO from technologie WHERE IDTECHNO = %d', $techno_id ) );
		return reset( $result )->NOMTECHNO;
	}
	
	public static function get_techno_by_conf( $conf_id = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( sprintf( 'SELECT IDTECHNO from utiliser WHERE IDCONFERENCE = %d', $conf_id ) );
		$techno_id = reset( $result )->IDTECHNO;
		return \Techno::get_techno( $techno_id );
	}
	
}