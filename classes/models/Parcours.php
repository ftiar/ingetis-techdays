<?php

class Parcours {

	function __construct() {
	}

	public static function get_all_parcours() {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( 'SELECT * from parcours' );
	}
	
	public static function get_parcours( $parcours_id = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( sprintf( 'SELECT LIBELLEPARCOURS from parcours WHERE IDPARCOURS = %d', $parcours_id ) );
		return reset( $result )->LIBELLEPARCOURS;
	}
	
}