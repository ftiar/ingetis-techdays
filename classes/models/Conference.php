<?php

class Conference {

	private $IDCONFERENCE = 0;
	private $SELECTION = 0;
	private $IDTHEME = 0;
	private $IDNIVEAU = 0;
	private $TITRE = '';
	private $CONTENU = '';
	private $NBVOTES = 0;

	function __construct( $id ) {
		$this->id = $id;

		$db = new \DB( 'ingetis_techdays' );
		$conf = $db->query( sprintf( 'SELECT * from conference WHERE conference.IDCONFERENCE = %d', $id ) );

		foreach ( get_object_vars( reset( $conf ) ) as $key => $value ) {
			$this->$key = $value;
		}

	}

	public static function add_conference() {
		if ( ! isset( $_POST['confTitle'] ) ) {
			return false;
		}

		$db = new \DB( 'ingetis_techdays' );
		
		// Add conference
		$req = $db->get_PDO()->prepare('INSERT INTO conference(SELECTION, IDTHEME, IDNIVEAU, TITRE, CONTENU) VALUES( :selection, :idtheme, :idniveau, :titre, :contenu )');
		$req->execute(array(
			'selection' => 0,
			'idtheme' => $_POST['confTheme'],
			'idniveau' => $_POST['confLevel'],
			'titre' => $_POST['confTitle'],
			'contenu' => $_POST['confDesc'],
		));
		
		$conf_id = $db->get_PDO()->lastInsertId();
		
		// Add all related data in DB that is linked to this conference
		\Conference::link_techno_to_conference( $conf_id, $_POST['confTools'] );
		\Conference::link_job_to_conference( $conf_id, $_POST['confJob'] );
		$conferencier_id = \Conference::add_conferencier( $_POST['confFirm'], $_POST['confName'],$_POST['confFirstname'],$_POST['confTel'],$_POST['confMail'] );
		\Conference::link_conferencier_to_conference( $conf_id, $conferencier_id );
			
		return new Conference( $conf_id );
	}
	
	public static function add_conferencier( $firmid = 0, $name = '', $firstname = '', $tel = '', $mail = '' ) {
		$db = new \DB( 'ingetis_techdays' );
		$req_tools = $db->get_PDO()->prepare('INSERT INTO conferencier(IDENTREPRISE, NOM, PRENOM, MAIL, TELEPHONE) VALUES( :firmid, :name, :firstname, :mail, :tel )');
		$req_tools->execute(array(
			'firmid' => (int) $firmid,
			'name' => $name,
			'firstname' => $firstname,
			'mail' => $mail,
			'tel' => $tel,
		));
		
		return $db->get_PDO()->lastInsertId();
	}
	
	public static function link_techno_to_conference( $confid = 0, $technoid = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		$req_tools = $db->get_PDO()->prepare('INSERT INTO utiliser(IDCONFERENCE, IDTECHNO) VALUES( :confid, :technoid )');
		$req_tools->execute(array(
			'confid' => (int) $confid,
			'technoid' => (int) $technoid,
		));
	}
	
	public static function link_conferencier_to_conference( $confid = 0, $conferencierid = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		$req_tools = $db->get_PDO()->prepare('INSERT INTO jouer(IDCONFERENCE, IDCONFERENCIER) VALUES( :confid, :conferencierid )');
		$req_tools->execute(array(
			'confid' => (int) $confid,
			'conferencierid' => (int) $conferencierid,
		));
	}
	
	public static function link_job_to_conference( $confid = 0, $jobid = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		$req_tools = $db->get_PDO()->prepare('INSERT INTO concerner(IDCONFERENCE, IDMETIER) VALUES( :confid, :jobid )');
		$req_tools->execute(array(
			'confid' => (int) $confid,
			'jobid' => (int) $jobid,
		));
	}

	public static function get_conference( $id ) {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( sprintf( 'SELECT * from conference WHERE conference.IDCONFERENCE = %d', $id ) );
	}
	
	public static function get_conferences() {
		$db = new \DB( 'ingetis_techdays' );
		return $db->query( 'SELECT * from conference' );
	}
	
	public static function set_selection( $confid = 0 ) {
		$db = new \DB( 'ingetis_techdays' );
		
		$result = $db->query( 'SELECT SELECTION from conference' );
		$selection = reset( $result )->SELECTION;
		
		$new_selection = 0;
		if ( $selection == 0 ) {
			$new_selection = 1;
		}
		
		return $db->query( sprintf( 'UPDATE conference SET SELECTION = %d WHERE IDCONFERENCE = %d;', $new_selection, $confid ) );
	}

}
