<?php
/**
 * Created by PhpStorm.
 * User: TFLOR
 * Date: 06/12/2016
 * Time: 14:38
 */

class DB {

	private $dbname = '';
	private $servername = '';
	private $username = '';
	private $password = '';
	private $charset = '';
	protected $pdo = null;

	function __construct( $dbname, $servername = 'localhost', $username = 'root', $password = '', $charset = 'UTF8' ) {
		if ( empty( $dbname ) ) {
			die('Error on DB conection, please give DB name when you instanciate the DB object' );
		}

		$this->servername = $servername;
		$this->username   = $username;
		$this->password   = $password;
		$this->dbname     = $dbname;
		$this->charset    = $charset;
	}

	public function get_PDO() {
		// If PDO is already instanciated, reuse it
		if ( $this->pdo != null && is_a( $this->pdo, 'PDO' ) ) {
			return $this->pdo;
		}

		try {
			$dsn = sprintf( 'mysql:dbname=%s;host=%s;charset=%s', $this->dbname, $this->servername, $this->charset );
			$pdo = new \PDO( $dsn, $this->username, $this->password );
			$pdo->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
			$this->pdo = $pdo;
			return $pdo;
		} catch ( \PDOException $e ) {
			$this->pdo = null;
			return $e;
		}

	}

	public function query( $statement = '', $class_name = null ) {
		if ( empty($statement) ) {
			return false;
		}

		$req = $this->get_PDO()->query( $statement );
		if ( $req == false ) {
			return false;
		}

		if ( $class_name == null ) {
			$data = $req->fetchAll( \PDO::FETCH_OBJ );
		} else {
			$data = $req->fetchAll( \PDO::FETCH_CLASS, $class_name );
		}
		if ( is_array($data) && ! empty($data) ) {
			return $data;
		} else {
			return false;
		}
	}

	public function exec( $statement = '' ) {
		if ( empty($statement) ) {
			return false;
		}
		$count = $this->get_PDO()->exec( $statement );
		return $count;
	}


}