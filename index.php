<?php include 'blocks/header.php'; ?>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
	<div class="container">
		<h1>Participez aux Microsoft Techdays !</h1>
		<p>Vous pouvez consulter les conférences qui seront proposées, voter pour une conférence, ou proposer votre propre conférence !</p>
		<p><a class="btn btn-primary btn-lg" href="pages/add-conf.php" role="button">Proposer une conférence &raquo;</a></p>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-xs-12" id="presentation">
			<h2>3 jours de conférences inédites !</h2>
			<p>Les techdays, c'est 3 jours, 18 000 visiteurs, invités, et intervenants d'entreprises et de domaines différents. C'est aussi plus de 300 sessions, conférences, ateliers et tables rondes. Le palais des Congrès s'habille des couleurs de Microsoft et de ses partenaires. Développeurs, professionnels de l'informatique, vous découvrirez les dernières innovations développées autour des technologies Microsoft.</p>
			<p><a class="btn btn-default btn-primary" href="#" role="button">Voir les conférences &raquo;</a></p>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
	  <div class="col-sm-6 col-md-4">
	    <div class="thumbnail">
	      <img src="http://lorempicsum.com/futurama/350/350/1" alt="...">
	      <div class="caption">
	        <h3>Thumbnail label</h3>
	        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
	        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-6 col-md-4">
	    <div class="thumbnail">
	      <img src="http://lorempicsum.com/futurama/350/350/2" alt="...">
	      <div class="caption">
	        <h3>Thumbnail label</h3>
	        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
	        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-6 col-md-4">
	    <div class="thumbnail">
	      <img src="http://lorempicsum.com/futurama/350/350/3" alt="...">
	      <div class="caption">
	        <h3>Thumbnail label</h3>
	        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
	        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-xs-12" id="inscription">
			<h2>Pour voter, rien de plus simple !</h2>
			<p>Inscrivez-vous directement sur notre site internet, et vous pourrez voter pour les conférences qui vous plaisent le plus..</p>
			<p><button type="button" data-toggle="modal" data-target="#inscriptionModal" class="btn btn-default btn-primary" href="#" role="button">Je m'inscris &raquo;</button></p>
		</div>
	</div>
</div>

<div class="modal fade" id="inscriptionModal" tabindex="-1" role="dialog" aria-labelledby="inscriptionModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Fermer"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="inscriptionModalLabel">Inscription</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="firstname-text" class="control-label">Prénom :</label>
            <input type="text" class="form-control" id="firstname-text" placeholder="Maxime...">
          </div>
          <div class="form-group">
            <label for="email-text" class="control-label">Email :</label>
            <input type="email" class="form-control" id="email-text" placeholder="maxime@dupont.fr">
          </div>
          <div class="form-group">
            <label for="password-text" class="control-label">Mot de passe :</label>
            <input type="password" class="form-control" id="password-text" placeholder="maxime@dupont.fr">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary">Je m'inscris</button>
      </div>
    </div>
  </div>
</div>

<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-md-4">
			<h2>Heading</h2>
			<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
			<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
		</div>
		<div class="col-md-4">
			<h2>Heading</h2>
			<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
			<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
		</div>
		<div class="col-md-4">
			<h2>Heading</h2>
			<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
			<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
		</div>
	</div>
</div>

<?php include 'blocks/footer.php'; ?>
