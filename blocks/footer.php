
<div class="container">
    <hr>

    <footer>
        <p>&copy; 2017 Florian TIAR.</p>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo DIR_URL; ?>js/jquery-3.2.1.min.js"></script>
<script src="<?php echo DIR_URL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo DIR_URL; ?>js/script.js"></script>
</body>
</html>
