<?php
session_start();

define( 'DIR_URL', 'http://127.0.0.1/ingetis_techdays/' );
define( 'DIR_PATH', 'C:\xampp\htdocs\ingetis_techdays\\' );
require_once DIR_PATH . 'inc\functions.php';

// Check login
if ( isset( $_POST['login'] ) && isset( $_POST['pwd'] ) ) {
    login( $_POST['login'], $_POST['pwd'] );
}

if ( isset( $_POST['logout'] ) ) {
	logout();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description"
          content="Les microsoft tech days est l'événement annuel à ne pas rater : Au programme, des conférences uniques d'experts du domaine">
    <meta name="author" content="Florian TIAR">

    <title>Microsoft TechDays - Florian TIAR</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo DIR_URL; ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo DIR_URL; ?>css/style.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Ouvrir/Fermer la navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo DIR_URL; ?>">Microsoft Techdays</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo DIR_URL; ?>">Accueil</a></li>
                <li><a href="<?php echo DIR_URL; ?>pages/add-conf.php">Proposer une conférence</a></li>
                <li><a href="<?php echo DIR_URL; ?>pages/catalogue.php">Catalogue</a></li>
                <li><a href="#">Analyse</a></li>
            </ul>

            <?php if ( ! is_logged_in() ) : ?>
                <form class="navbar-form navbar-right" method="POST">
                    <div class="form-group">
                        <input type="text" name="login" placeholder="Login" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <input type="password" name="pwd" placeholder="Mot de passe" class="form-control input-sm">
                    </div>
                    <button type="submit" class="btn btn-sm">Connexion</button>
                </form>
            <?php else : ?>
                <form class="navbar-form navbar-right" method="POST">
                    <button type="submit" name="logout" class="btn btn-sm">Deconnexion</button>
                </form>
            <?php endif; ?>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
